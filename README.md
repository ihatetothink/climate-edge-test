# ClimateEdge Test (AGUSTI BAU)

## Cloudformation
I believe all infrastructure should be defined in code. It makes infrastructure reproductible, maintainable and tracked through CVS. That's why I opted for a cloudformation solution.


## Files
The solution to the challenge is composed of few files:
### create-deployment-pipeline.sh
This file is responsible for managing the dependencies between the multiple cloudformation files, once the variables are set, executing it creates all the infrastructure required.
(I like that the repo credentials are stored as an ARN so that secrets are not exposed to the git repo!)

### cloudformation/
All the cloudformation files are in this folder. I split it into 3 because:

- We might want to have different identical deployments but only one pipeline (for example to have multiple staging envs, but ony one production environment listening for commits and all that)

- S3 is more comfortable to have it in a separate stack as when trying to delete a stack with non-empty s3 buckets, aws cli will throw an error and require accessing aws website.
 
### buildspec.yml
Defines the build steps that are performed to deploy the app. Exposes multiple artifacts and ensures tests are passed.


## How to run
To run this solution a few steps need to be made. Mainly setting few variables in `create-deployment-pipeline.sh`.

- AWS CLI is required to be running and configured on the machine.

- First, since we are using a Bitbucket repo. We need to create a "aws codestar connection". Information on how to do so, can be found [here](https://docs.aws.amazon.com/codepipeline/latest/userguide/connections-create.html). The resulting ARN should be copied to CONNECTIONARN vaiable.

- BRANCH_NAME and REPO_NAME should be filled in as well. REPO_NAME has the shape <username>/<reponame>

- STACKNAMES can be customized if wanted

- We can run the script like so: `./create-deployment-pipeline.sh`

- When deploying, the codestar connection usually fails on the first run for some reason. Accessing the codepipeline and retrying the source step usually fixes the issue

## How to check
There will be 4 stacks created after the script is ran.

- The S3 stack, will contain the buckets for the Swagger and Docs files.

- The EB stack will contain the elastic beanstalk stack that runs the application.




# Fun fact
While doing this challenge I found an [unresolved issue](https://stackoverflow.com/questions/50487060/codepipeline-with-terraform-and-beanstalk/62123732#62123732) on stackoverflow dealing with the same situation. I answered the solution two years after it was asked. 

I'm aware the commit history of this repo is a bit messy for the few files there are. I would make it tidier if working on a shared codebase
