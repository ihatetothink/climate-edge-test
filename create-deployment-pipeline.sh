#!/bin/bash

PROJECT_NAME="ClimateEdge-test"
CONNECTIONARN="arn:aws:codestar-connections:eu-west-1:278294596995:connection/c6c36568-7e94-47fe-89ad-338b6abe4cd1"
BRANCH_NAME="master"
REPOSITORY_NAME="ihatetothink/climate-edge-test"

S3_STACKNAME="CI-S3"
EB_STACKNAME="CI-EB"
CODEPIPELINE_STACKNAME="CI-CODEPIPELINE"

echo "Creating s3 stack..."
aws cloudformation deploy \
--template-file ./cloudformation/s3.yaml \
--stack-name $S3_STACKNAME   \

echo "Creating Elastic Beanstalk stack..."
aws cloudformation deploy \
--template-file ./cloudformation/beanstalk-environment.yaml \
--stack-name $EB_STACKNAME \

echo "Creating CodePipeline stack..."
aws cloudformation deploy \
--template-file ./cloudformation/codepipeline.yaml \
--stack-name $CODEPIPELINE_STACKNAME \
--capabilities CAPABILITY_IAM \
--parameter-overrides \
"ProjectName=$PROJECT_NAME" \
"ConnectionArn=$CONNECTIONARN" \
"BranchName=$BRANCH_NAME" \
"RepositoryName=$REPOSITORY_NAME" \
"S3StackName=$S3_STACKNAME" \
"EBStackName=$EB_STACKNAME" \

